var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

const readCallBack = (index, time) => {
  readBooksPromise(time, books[index])
      .then(times => {
        return index + readCallBack(index + 1, times)
      })
      .catch(err => {
        return err
      })
}

readCallBack(0, 10000)
