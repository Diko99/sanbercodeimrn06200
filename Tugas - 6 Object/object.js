//Soal no.1
console.log('======No.1 Array to object======\n')
var now = new Date()
var thisYear = now.getFullYear()

const arrayToObject = arr => {
  let obj = {
    firstName: '',
    lastName: '',
    gender: 0,
  }
  arr.map(item => {
    let getAge =  thisYear - item[3]
    let setAge = getAge === 45 ? getAge : thisYear < item[3] ? 'Invalid Birth Day' : getAge === 40 ? getAge : 'Invalid Birth Day'

    const data = {...obj}
    data.firstName = item[0]
    data.lastName = item[1]
    data.gender = item[2]
    data.age = setAge
    console.log(data)
  })
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
console.log('=================================\n')




console.log('======No.2 Shooping Time======\n')

function shoppingTime(memberId, money) {
  let listPurchased =  [ 'Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone' ]
  let data = {
    memberId:  memberId,
    money: money,
    listPurchased: memberId === '82Ku8Ma742' && money >= 170000 ? listPurchased.splice(4) : listPurchased,
    changeMoney: memberId === '82Ku8Ma742' ? 120000 : 0,
  }
  if (memberId && money) {
    if(memberId && money < 170000) {
      return 'Mohon maaf, uang anda tidak cukup'
    }
    return data
  } else if(memberId === '' || money) {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  } else {
    return 'Mohon maaf, toko X hanya berlaku untuk member saja'
  }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime(), '\n'); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log('=================================\n')



console.log('======No.3 Naik Angkot======\n')

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  //your code here
  const result = []
  arrPenumpang.map(item => {
    let pay = [2000, 8000]
    let conPay = rute[0] === item[1] ? pay[0] : pay[1]
    data = {
      penumpang: item[0],
      naikDari: item[1],
      tujuan: item[2],
      biaya: conPay
    }
    result.push(data)
  })
  return result
}

console.log(naikAngkot(
  [
    ['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']
  ]
))