// 1. Animal Class 

class Animal {
  // Code class di sini
  constructor(name) {
    this.name = name
    this.legs = 4
    this.cold_blooded = false
  }
}

var sheep = new Animal("shaun");
console.log('=========== 1. Animal Class  ==========')
console.log('=========== Release 1  ==========')

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
  constructor(name) {
    super(name)
    this.legs = 2
    this._yell = 'Auoo'
  }
  yell() {
    console.log(this._yell)
  }
}

class Frog extends Animal {
  constructor(name) {
    super(name)
    this._jump = 'hop-hop'
  }
  jump() {
    console.log(this._jump);
  }
}

console.log('=========== Release 2  ==========')
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log('=========== end 1 ========== \n')



console.log('=========== 2. Function to Class ==========')
class Clock {  
  constructor(template){
      this._template = template
      this._timer
  }
  

  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = this._template.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this._timer);
  }

  start() {
    this.render();
    this._timer = setInterval(() => this.render(), 1000);
  };
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 